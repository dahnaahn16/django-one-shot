from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def edit_item(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)
        if form.is_valid():
            instance_id = form.cleaned_data["list"].id
            form.save()
            return redirect("todo_list_detail", id=instance_id)
    else:
        form = TodoItemForm(instance=post)
    context = {
        "form": form,
    }
    return render(request, "todos/todo_item_update.html", context)


def show_todo_list(request):
    todo_object = TodoList.objects.all()
    context = {
        "show_todo_list": todo_object,
    }
    return render(request, "todos/todo_list_list.html", context)


def show_todo_list_detail(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todo_list_detail
    }
    return render(request, "todos/todo_list_detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoListForm()
    context = {
        "post_form": form,
    }
    return render(request, "todos/todo_list_create.html", context)


def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "post_form": form
    }
    return render(request, "todos/todo_item_create.html", context)


def edit_list(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=post)
    context = {
        "post_object": post,
        "post_form": form,
    }
    return render(request, "todos/todo_list_update.html", context)


def delete_list(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")
    return render(request, "todos/todo_list_delete.html")
